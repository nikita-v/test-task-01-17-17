const { resolve } = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry: [
    './index.jsx',
  ],
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx', '.scss'],
    alias: {
      components: resolve(__dirname, 'app/components'),
      containers: resolve(__dirname, 'app/containers'),
      store: resolve(__dirname, 'app/store'),
      settings: resolve(__dirname, 'settings.json'),
    },
  },
  context: resolve(__dirname, 'app'),
  devtool: 'inline-source-map',
  devServer: {
    contentBase: resolve(__dirname, 'dist'),
    publicPath: '/',
    stats: 'errors-only',
  },
  module: {
    rules: [
      {
        test: [/\.js$/, /\.jsx$/],
        use: [
          'babel-loader',
        ],
        include: /app/,
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          loader: 'css-loader?modules&camelCase!sass-loader',
        }),
        include: /app/,
      },
      {
        test: /\.png$/,
        use: 'url-loader',
        include: /app/,
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('styles.css'),
  ],
}
