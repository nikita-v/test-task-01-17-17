import { random } from 'faker'

export default () =>
  new Promise(res => res(Object.assign(
    ...['veryHight', 'hight', 'moderate', 'low', 'veryLow'].map(type => ({
      [type]: random.number(9),
    })),
  )))
