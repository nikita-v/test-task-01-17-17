import { address, random } from 'faker'

// Generate the array with fake top locations
const topLocations = []

for (let i = 1; i <= 5; i += 1) {
  topLocations.push({
    _id: i,
    city: address.city(),
    lat: address.latitude(),
    lng: address.longitude(),
    num: random.number({ min: 3, max: 20 }),
  })
}

export default () =>
  new Promise(res =>
    res(topLocations.sort((a, b) => (b.num - a.num))),
  )
