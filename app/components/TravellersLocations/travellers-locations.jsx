import React, { PropTypes } from 'react'
import { Tabs, Tab } from 'material-ui/Tabs'

import CurrentTopLocations from './CurrentTopLocations'
import TravellersChart from './TravellersChart'
import TravellersMap from './TravellersMap'
import styles from './styles'

const TravellersLocations = ({ activeTab, changeTab, topLocations }) => (
  <Tabs
    value={activeTab}
    onChange={tabName => changeTab(tabName)}
    tabItemContainerStyle={{ backgroundColor: '#d6d6d6' }}
    inkBarStyle={{ backgroundColor: '#0355d2' }}
  >
    <Tab label='Current' value='current' buttonStyle={{ color: '#232323' }}>
      <div className={styles.grid}>
        <div className={styles.gridCol}>
          <TravellersChart chartData={topLocations} />
        </div>

        <div className={styles.gridCol}>
          <CurrentTopLocations topLocations={topLocations} />
        </div>

        <div className={styles.gridCol}>
          <TravellersMap markers={topLocations} />
        </div>
      </div>
    </Tab>

    <Tab label='Future' value='future' buttonStyle={{ color: '#232323' }}>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
    </Tab>
  </Tabs>
)

TravellersLocations.propTypes = {
  activeTab: PropTypes.string.isRequired,
  changeTab: PropTypes.func.isRequired,
  topLocations: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}

export default TravellersLocations
