import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { address, random } from 'faker'
import GoogleMap from 'google-map-react'

import TravellersMap from './travellers-map'
import Marker from './Marker'

describe('<TravellersMap />', () => {
  it('should show map and markers', () => {
    const markers = [{
      _id: 1,
      lat: address.latitude(),
      lng: address.longitude(),
      num: random.number(),
    }]
    const wrapper = shallow(<TravellersMap markers={markers} />)

    expect(wrapper.find(GoogleMap)).to.have.length(1)
    expect(wrapper.find(Marker)).to.have.length(markers.length)
  })
})
