import React, { PropTypes } from 'react'
import MapsPlace from 'material-ui/svg-icons/maps/place'

import styles from './styles'

const Marker = ({ label }) => (
  <div>
    <MapsPlace className={styles.markerIcon} />

    <span className={styles.markerLabel}>{label}</span>
  </div>
)

Marker.propTypes = {
  label: PropTypes.number.isRequired,
}

export default Marker
