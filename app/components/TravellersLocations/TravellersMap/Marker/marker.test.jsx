import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { random } from 'faker'
import MapsPlace from 'material-ui/svg-icons/maps/place'

import Marker from './marker'

describe('<TravellersMap />', () => {
  it('should MapsPlace show icon and span', () => {
    const wrapper = shallow(<Marker label={random.number()} />)

    expect(wrapper.find(MapsPlace)).to.have.length(1)
    expect(wrapper.find('span')).to.have.length(1)
  })
})
