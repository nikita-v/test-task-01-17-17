import React, { PropTypes } from 'react'
import GoogleMap from 'google-map-react'

import { GoogleMapsApiKey } from 'settings'
import Marker from './Marker'

const TravellersMap = ({ markers }) => (
  <GoogleMap
    bootstrapURLKeys={{ key: GoogleMapsApiKey, language: 'en' }}
    defaultCenter={{ lat: 0, lng: 0 }}
    defaultZoom={0}
  >
    {markers.map(marker => (
      <Marker
        key={marker._id}
        lat={marker.lat}
        lng={marker.lng}
        label={marker.num}
      />
    ))}
  </GoogleMap>
)

TravellersMap.propTypes = {
  markers: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}

export default TravellersMap
