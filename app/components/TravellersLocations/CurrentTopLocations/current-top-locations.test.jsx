import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { List, ListItem } from 'material-ui/List'

import getTopLocations from '../../../utils/getTopLocations'
import CurrentTopLocations from './current-top-locations'

describe('<CurrentTopLocations />', () => {
  it('should show <List /> with 5 <ListItem />', () => {
    getTopLocations().then((res) => {
      const wrapper = shallow(<CurrentTopLocations topLocations={res} />)

      expect(wrapper.find(List)).to.have.length(1)
      expect(wrapper.find(ListItem)).to.have.length(5)
    })
  })
})
