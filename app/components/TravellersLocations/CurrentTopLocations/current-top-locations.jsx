import React, { PropTypes } from 'react'
import { List, ListItem } from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import Divider from 'material-ui/Divider'
import AVPlayArror from 'material-ui/svg-icons/av/play-arrow'

import styles from './styles'

const CurrentTopLocations = ({ topLocations }) => (
  <div>
    <List>
      {topLocations.map((location, i) => (
        <div key={location._id}>
          <ListItem
            primaryText={location.city}
            leftAvatar={
              <Avatar size={20} backgroundColor='#0c94f6' >
                {location.num}
              </Avatar>
            }
            className={styles.listItem}
            innerDivStyle={{ padding: '.5rem 0 .5rem 52px' }}
            rightIcon={<AVPlayArror className={styles.listItemIcon} />}
          />

          {i + 1 < topLocations.length
          && <Divider inset style={{ marginLeft: '52px' }} />}
        </div>
      ))}
    </List>
  </div>
)

CurrentTopLocations.propTypes = {
  topLocations: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.number.isRequired,
    city: PropTypes.string.isRequired,
    lat: PropTypes.string.isRequired,
    lng: PropTypes.string.isRequired,
    num: PropTypes.number.isRequired,
  })).isRequired,
}

export default CurrentTopLocations
