import React, { PropTypes } from 'react'
import { Line as LineChart } from 'react-chartjs'

import styles from './styles'

const TravellersChart = ({ chartData }) => (
  <div>
    <span className={styles.label}>
      {chartData.map(v => (v.num)).reduce((a, b) => (a + b))}
    </span>

    <LineChart
      data={{
        labels: chartData.map(v => (v.city.substring(0, 3))),
        datasets: [{
          labels: 'Travellers Chart',
          fillColor: 'transparent',
          strokeColor: '#444444',
          pointColor: '#444444',
          data: chartData.map(v => (v.num)),
        }],
      }}
      width='150'
      className={styles.chart}
    />
  </div>
)

TravellersChart.propTypes = {
  chartData: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.number.isRequired,
    city: PropTypes.string.isRequired,
    lat: PropTypes.string.isRequired,
    lng: PropTypes.string.isRequired,
    num: PropTypes.number.isRequired,
  })).isRequired,
}

export default TravellersChart
