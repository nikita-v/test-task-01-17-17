import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { Line as LineChart } from 'react-chartjs'

import TravellersChart from './travellers-chart'
import getTopLocations from '../../../utils/getTopLocations'

describe('<TravellersChart />', () => {
  it('should show LineChart with label', () => {
    getTopLocations().then((res) => {
      const wrapper = shallow(<TravellersChart chartData={res} />)

      expect(wrapper.find(LineChart)).to.have.length(1)
      expect(wrapper.find('span').text())
        .to.equal(res.map(v => (v.num)).reduce((a, b) => (a + b)).toString())
    })
  })
})
