import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { Tabs, Tab } from 'material-ui/Tabs'

import getTopLocations from '../../utils/getTopLocations'
import CurrentTopLocations from './CurrentTopLocations'
import TravellersLocations from './travellers-locations'
import TravellersMap from './TravellersMap'
import TravellersChart from './TravellersChart'

describe('<TravellersLocations />', () => {
  let wrapper

  beforeEach(() => (
    getTopLocations().then(res => (
      wrapper = shallow(<TravellersLocations
        topLocations={res}
        activeTab='current'
        changeTab={() => (null)}
      />)
    ))
  ))

  it('should show <Tabs /> with 2 <Tab />', () => {
    expect(wrapper.find(Tabs)).to.have.length(1)
    expect(wrapper.find(Tab)).to.have.length(2)
  })

  it('should show TopLocations, Chart and Map on the first tab', () => {
    const firstTab = wrapper.find(Tab).first()

    expect(firstTab.find(CurrentTopLocations)).to.have.length(1)
    expect(firstTab.find(TravellersMap)).to.have.length(1)
    expect(firstTab.find(TravellersChart)).to.have.length(1)
  })
})
