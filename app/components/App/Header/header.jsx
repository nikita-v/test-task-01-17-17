import React, { PropTypes } from 'react'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import NavigationMenu from 'material-ui/svg-icons/navigation/menu'
import SocialPerson from 'material-ui/svg-icons/social/person'

import Loading from 'components/Loading'
import styles from './styles'
import AppHeaderSearch from './Search'

const AppHeader = ({ alerts, toggleNav }) => (
  <Toolbar className={styles.root}>
    <ToolbarGroup className={styles.firstChild} firstChild>
      <IconButton
        onTouchTap={toggleNav}
      >
        <NavigationMenu color='#ebebeb' />
      </IconButton>

      <AppHeaderSearch />
    </ToolbarGroup>

    <ToolbarGroup className={styles.lastChild} lastChild>
      {alerts ? <div className={styles.lastChildBlock}>
        <span className={styles.span}>Current alerts</span>

        <Avatar className={styles.badge} size={20} backgroundColor='#bb0500'>
          {alerts.veryHight}
        </Avatar>
        <Avatar className={styles.badge} size={20} backgroundColor='#e04a03'>
          {alerts.hight}
        </Avatar>
        <Avatar className={styles.badge} size={20} backgroundColor='#ffa822'>
          {alerts.moderate}
        </Avatar>
        <Avatar className={styles.badge} size={20} backgroundColor='#f4ed37'>
          {alerts.low}
        </Avatar>
        <Avatar className={styles.badge} size={20} backgroundColor='#8ed569'>
          {alerts.veryLow}
        </Avatar>
      </div> : <Loading />}

      <div className={styles.lastChildBlock}>
        <span className={styles.span}>Messages</span>

        <Avatar className={styles.badge} size={20} backgroundColor='#0355d2'>
          5
        </Avatar>
      </div>

      <div className={styles.lastChildBlock}>
        <span className={styles.span}>Your account</span>

        <Avatar icon={<SocialPerson />} size={22} />

      </div>
    </ToolbarGroup>
  </Toolbar>
)

AppHeader.propTypes = {
  alerts: PropTypes.shape({ // eslint-disable-line react/require-default-props
    veryHight: PropTypes.number.isRequired,
    hight: PropTypes.number.isRequired,
    moderate: PropTypes.number.isRequired,
    low: PropTypes.number.isRequired,
    veryLow: PropTypes.number.isRequired,
  }),
  toggleNav: PropTypes.func.isRequired,
}

export default AppHeader
