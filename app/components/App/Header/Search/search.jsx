import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import ArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down'
import Bookmark from 'material-ui/svg-icons/image/collections-bookmark'

import styles from './styles'

export default () => (
  <div className={styles.root}>
    <RaisedButton
      className={styles.button}
      backgroundColor='#8bcd77'
      label='Advanced search'
      labelPosition='before'
      labelColor='#23670c'
      labelStyle={{
        fontSize: '.8rem',
        fontWeight: 'bold',
        textTransform: null,
        paddingRight: null,
      }}
      buttonStyle={{ borderRadius: 0 }}
      icon={<ArrowDropDown className={styles.buttonIcon} />}
    />

    <TextField
      className={styles.textField}
      hintText='Find people, places, alerts...'
      underlineShow={false}
    />

    <RaisedButton
      backgroundColor='#606060'
      style={{ minWidth: '2rem' }}
      buttonStyle={{ borderRadius: 0 }}
      icon={<Bookmark color='#ebebeb' />}
    />
  </div>
)
