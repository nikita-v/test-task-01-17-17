import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'

import AppHeaderSearch from './search'

describe('<AppHeaderSearch />', () => {
  it('should show 2 buttons and a text field', () => {
    const wrapper = shallow(<AppHeaderSearch />)

    expect(wrapper.find(RaisedButton)).to.have.length(2)
    expect(wrapper.find(TextField)).to.have.length(1)
  })
})
