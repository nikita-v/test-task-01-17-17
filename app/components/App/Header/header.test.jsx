import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { random } from 'faker'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'

import { toggleNav } from 'store/App'
import AppHeader from './header'

describe('<AppHeader />', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(<AppHeader
      alerts={Object.assign(
        ...['veryHight', 'hight', 'moderate', 'low', 'veryLow'].map(type => ({
          [type]: random.number(10),
        })),
      )}
      toggleNav={toggleNav}
    />)
  })

  it('should show 6 badges and avatar', () => {
    expect(wrapper.find(Avatar)).to.have.length(7)
  })

  it('should show a menu button', () => {
    expect(wrapper.find(IconButton)).to.have.length(1)
  })
})
