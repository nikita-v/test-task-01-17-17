import React, { PropTypes } from 'react'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'

import ActionDashboard from 'material-ui/svg-icons/action/dashboard'
import AlertWarning from 'material-ui/svg-icons/alert/warning'
import MapsPlace from 'material-ui/svg-icons/maps/place'
import AlertAdd from 'material-ui/svg-icons/alert/add-alert'
import MapsFlight from 'material-ui/svg-icons/maps/flight'
import CommunicationMessage from 'material-ui/svg-icons/communication/message'
import ActionAccountCircle from 'material-ui/svg-icons/action/account-circle'

import styles from './styles'
import logo from './logo.png'

const AppNav = ({ open }) => (
  <Drawer
    open={open}
    width={250}
    containerClassName={styles.root}
  >
    <header className={styles.header}>
      <img className={styles.headerLogo} src={logo} alt='Logo' />
    </header>

    <MenuItem
      className={styles.item}
      leftIcon={<ActionDashboard className={styles.itemIcon} />}
    >Dashboard</MenuItem>

    <Divider className={styles.divider} />

    <MenuItem
      className={styles.item}
      leftIcon={<AlertWarning className={styles.itemIcon} />}
    >Risk alerts</MenuItem>
    <MenuItem
      className={styles.item}
      leftIcon={<MapsPlace className={styles.itemIcon} />}
    >Travellers</MenuItem>
    <MenuItem
      className={styles.item}
      leftIcon={<AlertAdd className={styles.itemIcon} />}
    >Compliance alerts</MenuItem>
    <MenuItem
      className={styles.item}
      leftIcon={<MapsFlight className={styles.itemIcon} />}
    >Flight delays</MenuItem>

    <Divider className={styles.divider} />

    <MenuItem
      className={styles.item}
      leftIcon={<CommunicationMessage className={styles.itemIcon} />}
    >Messages</MenuItem>
    <MenuItem
      className={styles.item}
      leftIcon={<ActionAccountCircle className={styles.itemIcon} />}
    >Community</MenuItem>
    <MenuItem className={styles.item} insetChildren>messages</MenuItem>

    <Divider className={styles.divider} />

    <MenuItem className={styles.item} insetChildren>Rule builder</MenuItem>
    <MenuItem className={styles.item} insetChildren>Saved searches</MenuItem>
    <MenuItem className={styles.item} insetChildren>Reporting history</MenuItem>

    <Divider className={styles.divider} />

    <MenuItem className={styles.item} insetChildren>Site admin</MenuItem>


    <MenuItem className={styles.item} insetChildren>Help</MenuItem>
    <MenuItem className={styles.item} insetChildren>Sign Out</MenuItem>
  </Drawer>
)

AppNav.propTypes = {
  open: PropTypes.bool.isRequired,
}

export default AppNav
