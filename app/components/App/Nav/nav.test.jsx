import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'

import AppNav from './nav'

describe('<AppNav />', () => {
  const wrapper = shallow(<AppNav open />)

  it('should render <Drawer />', () => {
    expect(wrapper.find(Drawer)).to.have.length(1)
  })

  it('should show 14 items', () => {
    expect(wrapper.find(MenuItem)).to.have.length(14)
  })
})
