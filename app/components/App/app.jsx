import React, { PropTypes } from 'react'

import AppHeader from './Header'
import AppNav from './Nav'
import './styles'

const App = ({ alerts, children, navOpened, toggleNav }) => (
  <div>
    <AppNav
      open={navOpened}
    />

    <div style={{ marginLeft: navOpened ? 250 : 0 }}>
      <AppHeader
        alerts={alerts}
        toggleNav={toggleNav}
      />

      {children}
    </div>
  </div>
)

App.defaultProps = {
  alerts: null,
}

App.propTypes = {
  alerts: PropTypes.shape(),
  children: PropTypes.node.isRequired,
  navOpened: PropTypes.bool.isRequired,
  toggleNav: PropTypes.func.isRequired,
}

export default App
