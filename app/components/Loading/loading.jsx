import React, { PropTypes } from 'react'
import CircularProgress from 'material-ui/CircularProgress'

import styles from './styles'

const Loading = ({ centered }) => (
  <CircularProgress
    className={centered && styles.centered}
    size={centered ? 50 : 30}
  />
)

Loading.defaultProps = {
  centered: false,
}

Loading.propTypes = {
  centered: PropTypes.bool,
}

export default Loading
