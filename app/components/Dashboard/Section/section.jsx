import React, { PropTypes } from 'react'
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import ActionSettings from 'material-ui/svg-icons/action/settings'

import styles from './styles'

const DashboardSection = ({ icon, title, children }) => (
  <Paper className={styles.root}>
    <header className={styles.header}>
      <IconButton className={styles.headerButton}>
        <ActionSettings className={styles.headerButtonIcon} />
      </IconButton>

      <h3 className={styles.headerTitle}>{icon}&nbsp;{title}</h3>
    </header>

    {children}
  </Paper>
)

DashboardSection.propTypes = {
  icon: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}

export default DashboardSection
