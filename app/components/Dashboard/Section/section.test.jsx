import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import { lorem } from 'faker'
import IconButton from 'material-ui/IconButton'
import AlertWarning from 'material-ui/svg-icons/alert/warning'

import DashboardSection from './section'

describe('<DashboardSection />', () => {
  it('should show <header /> with title, icon and button', () => {
    const header = shallow(
      <DashboardSection icon={<AlertWarning />} title={lorem.sentence()}>
        <div />
      </DashboardSection>,
    ).find('header')

    expect(header.find('h3')).to.have.length(1)
    expect(header.find(IconButton)).to.have.length(1)
    expect(header.find('h3').find(AlertWarning)).to.have.length(1)
  })
})
