import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import DashboardSection from 'components/Dashboard/Section'
import TravellersLocations from 'containers/TravellersLocations'
import DashboardHeader from 'components/Dashboard/Header'
import Dashboard from './dashboard'

describe('<Dashboard />', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(<Dashboard />)
  })

  it('should show <DashboardHeader />', () => {
    expect(wrapper.find(DashboardHeader)).to.have.length(1)
  })

  it('should show 5 <DashboardSection />', () => {
    expect(wrapper.find(DashboardSection)).to.have.length(5)
  })

  it('should show <TravellersLocations /> on the last section', () => {
    expect(
      wrapper.find(DashboardSection).at(4).find(TravellersLocations),
    ).to.have.length(1)
  })
})
