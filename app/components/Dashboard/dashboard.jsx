import React from 'react'
import AlertWarning from 'material-ui/svg-icons/alert/warning'
import MapsFlight from 'material-ui/svg-icons/maps/flight'
import AlertAdd from 'material-ui/svg-icons/alert/add-alert'
import MapsHotel from 'material-ui/svg-icons/maps/hotel'

import TravellersLocations from 'containers/TravellersLocations'
import styles from './styles'
import DashboardHeader from './Header'
import DashboardSection from './Section'

const Dashboard = () => (
  <div>
    <DashboardHeader />

    <div className={styles.grid}>
      <div className={styles.gridCol}>
        <DashboardSection icon={<AlertWarning />} title='Risk Alert'>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
        </DashboardSection>

        <DashboardSection icon={<MapsFlight />} title='Flight Status'>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
        </DashboardSection>
      </div>

      <div className={styles.gridCol}>
        <DashboardSection
          icon={<AlertWarning />}
          title='CSAR Traveller affected'
        >
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
        </DashboardSection>

        <DashboardSection icon={<AlertAdd />} title='Compliance alerts'>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
        </DashboardSection>
      </div>

      <div className={styles.gridCol}>
        <DashboardSection icon={<MapsHotel />} title='Travellers and locations'>
          <TravellersLocations />
        </DashboardSection>
      </div>
    </div>
  </div>
)

export default Dashboard
