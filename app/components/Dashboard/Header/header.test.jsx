import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'
import Paper from 'material-ui/Paper'
import ActionDashboard from 'material-ui/svg-icons/action/dashboard'
import FlatButton from 'material-ui/FlatButton'

import DashboardHeader from './header'

describe('<DashboardHeader />', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(<DashboardHeader />)
  })

  it('should show <h2> and the ActionDashboard icon in first <Paper />', () => {
    expect(wrapper.find(Paper).first().find('h2')).to.have.length(1)
    expect(wrapper.find(Paper).first().find(ActionDashboard)).to.have.length(1)
  })

  it('should show 2 <FlatButton /> in second <Paper />', () => {
    expect(wrapper.find(Paper).at(1).find(FlatButton)).to.have.length(2)
  })
})
