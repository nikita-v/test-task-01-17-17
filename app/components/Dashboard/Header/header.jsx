import React from 'react'
import Paper from 'material-ui/Paper'
import ActionDashboard from 'material-ui/svg-icons/action/dashboard'
import FlatButton from 'material-ui/FlatButton'
import ActionSettings from 'material-ui/svg-icons/action/settings'
import SocialShare from 'material-ui/svg-icons/social/share'

import styles from './styles'

export default () => (
  <header className={styles.root}>
    <Paper className={styles.paper}>
      <ActionDashboard className={styles.paperHeaderIcon} />

      <h2 className={styles.paperHeader}>Dashboard</h2>
    </Paper>

    <Paper className={styles.paper}>
      <FlatButton
        label='Dashboard settings'
        labelPosition='before'
        backgroundColor='#ebebeb'
        labelStyle={{ color: '#444444' }}
        icon={<ActionSettings color='#444444' />}
        style={{ height: '1.8rem', lineHeight: null }}
      />

      <FlatButton
        label='Share / Export'
        labelPosition='before'
        backgroundColor='#606060'
        labelStyle={{ color: '#ffffff' }}
        icon={<SocialShare color='#ffffff' />}
        style={{ height: '1.8rem', lineHeight: null }}
      />
    </Paper>
  </header>
)
