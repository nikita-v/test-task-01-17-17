import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'
import loggerMiddleware from 'redux-logger'

import app from 'store/App'
import travellersLocations from 'store/TravellersLocations'

const middlewares = [
  thunkMiddleware,
  loggerMiddleware(),
].filter(Boolean)

const createStoreWithMiddleware = applyMiddleware(
  ...middlewares,
)(createStore)

export default createStoreWithMiddleware(combineReducers({
  app,
  travellersLocations,
}), {})
