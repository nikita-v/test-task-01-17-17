import getAlertsUtil from '../../utils/getAlerts'

const TOGGLE_NAV = 'app/TOGGLE_NAV'
const GET_ALERTS_SUCCESS = 'app/GET_ALERTS_SUCCESS'

export default (state = {
  navOpened: false,
}, action) => {
  switch (action.type) {
    case TOGGLE_NAV:
      return { ...state, navOpened: !state.navOpened }
    case GET_ALERTS_SUCCESS:
      return { ...state, alerts: action.payload }
    default:
      return state
  }
}

export const toggleNav = () => ({ type: TOGGLE_NAV })

export const getAlerts = () => (dispatch) => {
  window.setTimeout(() => {
    getAlertsUtil().then(res => (dispatch({
      type: GET_ALERTS_SUCCESS,
      payload: res,
    })))
  }, 3000)
}
