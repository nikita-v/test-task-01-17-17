import { expect } from 'chai'

import appReducer, { toggleNav } from './index'

describe('App redux module', () => {
  it('should change navOpened param by use toggleNav()', () => {
    const initialState = { navOpened: false }
    const state = appReducer(initialState, toggleNav())

    expect(state).to.eql({ navOpened: !initialState.navOpened })
  })
})
