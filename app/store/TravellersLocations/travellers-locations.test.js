import { expect } from 'chai'

import travellersLocationsReducer, { changeTab } from './index'

describe('TravellersLocations redux module', () => {
  it('should change activeTab param by use changeTab()', () => {
    const initialState = { activeTab: 'current' }
    const state = travellersLocationsReducer(initialState, changeTab('future'))

    expect(state).to.eql({ activeTab: 'future' })
  })
})
