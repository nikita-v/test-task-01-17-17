const CHANGE_TAB = 'app/CHANGE_TAB'

export default (state = {
  activeTab: 'current',
}, action) => {
  switch (action.type) {
    case CHANGE_TAB:
      return { ...state, activeTab: action.payload }
    default:
      return state
  }
}

export const changeTab = tabName => ({ type: CHANGE_TAB, payload: tabName })
