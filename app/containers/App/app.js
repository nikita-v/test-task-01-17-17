import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose } from 'react-komposer'

import AppComponent from 'components/App'
import * as appActions from 'store/App'

export default connect(
  state => (state.app),
  dispatch => bindActionCreators(appActions, dispatch),
)(compose(({ getAlerts, alerts }, onData) => {
  if (!alerts) getAlerts()

  onData(null, {})
})(AppComponent))
