import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { compose } from 'react-komposer'

import { containerLoading as loadingHandler } from 'components/Loading'
import TravellersLocationsComponent from 'components/TravellersLocations'
import { changeTab } from 'store/TravellersLocations'
import getTopLocations from '../../utils/getTopLocations'

export default connect(
  state => (state.travellersLocations),
  dispatch => bindActionCreators({ changeTab }, dispatch),
)(compose((props, onData) => {
  window.setTimeout(() => {
    getTopLocations().then(topLocations => onData(null, { topLocations }))
  }, 1500)
}, {
  loadingHandler,
})(TravellersLocationsComponent))
