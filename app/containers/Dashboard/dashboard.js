import { compose } from 'react-komposer'

import DashboardComponent from 'components/Dashboard'

export default compose((props, onData) => {
  onData(null, props)
})(DashboardComponent)
